from scipy.optimize import linprog

n = 12

# -1*x1 -   x2 <= -2*n
#    x1 - 2*x2 <=    n
# -3*x1 + 2*x2 <=  2*n
# -1*x1 -   x2 <= -5*n
# -1*x1 + 3*x2 <=  6*n

lhs_ineq = [[-1, -1],
            [1, -2],
            [-3, 2],
            [-1, -1], #для 20 варианта
            [-1, 3]]  #для 20 варианта
            #[3, -1],  #для 7 варианта
            #[-1, 3]]  #для 7 варианта

rhs_ineq = [-2*n,
            n,
            2*n,
            -5*n, #для 20 варианта
            6*n]  #для 20 варианта
            #2*n,  #для 7 варианта
            #6*n]  #для 7 варианта


bnd = [(0, 4*n),  # Границы x1
       (0, 3*n)]  # Границы x2

f = [-3, 1] #для 20 варианта
min_f = [3, -1] # -f, так как решаем задачу минимализации #для 20 варианта

#f = [1, 1]  #для 7 варианта
#min_f = [-1, -1]  # -f, так как решаем задачу минимализации #для 7 варианта

opt = linprog(c=min_f, A_ub=lhs_ineq, b_ub=rhs_ineq,
              method="simplex")
print(opt.message)
print('Optimal point: ', opt.x)

f1 = lhs_ineq[3][0] * opt.x[0] + lhs_ineq[3][1] * opt.x[1]
f2 = min_f[0] * opt.x[0] + min_f[1] * opt.x[1]
f3 = lhs_ineq[4][0] * opt.x[0] + lhs_ineq[4][1] * opt.x[1]

print('F1(P) = ', -f1)
print('F2(P) = ', -f2)
print('F3(P) = ', -f3)
