### Matrix Definition

The input matrix is defined in the script as follows:
```python
Matrix = [
    [[5, 10], [2, 7],  [8, 6], [5, 9]],
    # ... (other decision alternatives)
]
```

### First Task - Vector Minimax

- The function `vec_minimax` calculates the vector minimax values for each row in the matrix.
- The function `pareto` identifies the Pareto-optimal solutions based on vector minimax values.

### Second Task - Vector Minimax Regret

- The function `vec_minimax_regret` calculates the vector minimax regret values for each column in the matrix.
- The function `pareto` identifies the Pareto-optimal solutions based on vector minimax regret values.

### Third Task - Decision Criteria Evaluation

- The script evaluates four decision criteria: Wald, Savage, Hurwicz, and Laplace, for each criterion associated with the first attribute of the alternatives (`x` values).
- The functions `vald`, `savidzh`, `gurvits`, and `laplas` implement these criteria.

### Fourth Task - Decision Criteria Evaluation (Second Attribute)

- Similar to the third task, this section evaluates the four decision criteria for the second attribute of the alternatives (`y` values).

### Summary

The script generates a summary table displaying the results for each decision criterion and alternative. The best choice is determined based on the cumulative scores.

## Output

After running the script, you will see a summary table printed in the console, along with the best choice(s) based on the given decision criteria.

Feel free to modify the matrix and explore different scenarios by adjusting the input data.
